Hi there.

I know you asked for a well-designed solution rather than a quick one.
However, my resources are limited, so I will describe what I skipped in order to finish it quicker.

1. Tests. I did not create any. All logic should be covered with unit tests. There should be integration tests as well.
2. I did not really use Reactive Web features. If you need that in your project, I will spend some time to research it first, instead of doing it rapidly right now.
3. You asked about security when calling the exchange provider. I used the RestTemplate which provides pretty agile way of handling that - manually or through interceptors, so I don't see a big challenge in that.
4. Exception classes and exception handling is skipped, but it is required in real life :)
5. I used the default .gitignore, so there might some garbage, sorry for that.

Modules structure and logic provided with idea of easy maintenance of provided-related changes.

cURL for testing:
`curl --location --request GET 'localhost:8080/currency/convert' \
--header 'Content-Type: application/json' \
--data-raw '{
"from": "EUR",
"to": "USD",
"amount": 100
}'`
