package com.example.currency.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class ClientResponse implements Serializable {

    private Map<String, BigDecimal> rates = new HashMap<>();

}
