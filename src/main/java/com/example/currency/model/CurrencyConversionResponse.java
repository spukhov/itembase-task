package com.example.currency.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CurrencyConversionResponse implements Serializable {
    private String from;
    private String to;
    private BigDecimal amount;
    private BigDecimal converted;
}
