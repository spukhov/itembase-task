package com.example.currency.client;

import com.example.currency.model.ClientResponse;
import com.example.currency.model.CurrencyConversionRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

@Slf4j
public abstract class ConversionClient {

    private final RestTemplate restTemplate;

    public ConversionClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ClientResponse getRates(CurrencyConversionRequest request) {
        log.info(String.format("Making request to provider %s", getProviderName()));
        return restTemplate.getForObject(getExchangeUrl(request), ClientResponse.class);
    }

    protected abstract String getExchangeUrl(CurrencyConversionRequest request);

    public abstract String getProviderName();
}
