package com.example.currency.config;

import com.example.currency.client.ConversionClient;
import com.example.currency.service.CurrencyConversionService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Arrays;

@Configuration
public class AppConfig {

    @Resource
    private ConversionClient ioConversionClient;
    @Resource
    private ConversionClient comConversionClient;

    @Bean
    public CurrencyConversionService currencyConversionService() {
        return new CurrencyConversionService(Arrays.asList(ioConversionClient, comConversionClient));
    }
}
