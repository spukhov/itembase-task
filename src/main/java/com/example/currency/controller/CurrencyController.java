package com.example.currency.controller;

import com.example.currency.model.CurrencyConversionRequest;
import com.example.currency.model.CurrencyConversionResponse;
import com.example.currency.service.CurrencyConversionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
// todo add tests
public class CurrencyController {

    @Resource
    private CurrencyConversionService currencyConversionService;

    @GetMapping("/currency/convert")
    public CurrencyConversionResponse getConversion(@RequestBody CurrencyConversionRequest request) {
        return currencyConversionService.convert(request);
    }

}
