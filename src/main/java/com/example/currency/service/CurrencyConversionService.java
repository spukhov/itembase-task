package com.example.currency.service;

import com.example.currency.client.ConversionClient;
import com.example.currency.model.ClientResponse;
import com.example.currency.model.CurrencyConversionRequest;
import com.example.currency.model.CurrencyConversionResponse;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
// todo add tests
public class CurrencyConversionService {

    private final List<ConversionClient> clients;

    public CurrencyConversionService(List<ConversionClient> clients) {
        this.clients = clients;
    }

    public CurrencyConversionResponse convert(CurrencyConversionRequest request) {

        ClientResponse clientResponse = getClientResponse(request);

        Map<String, BigDecimal> rates = clientResponse.getRates();
        BigDecimal rate = rates.get(request.getTo());
        if (rate == null) {
            // todo add proper exceptions and exception handling
            throw new RuntimeException(String.format("There is no exchange rate for currency %s", request.getFrom()));
        }
        CurrencyConversionResponse response = new CurrencyConversionResponse();
        BigDecimal amount = request.getAmount();
        response.setAmount(amount);
        response.setFrom(request.getFrom());
        response.setTo(request.getTo());
        response.setConverted(amount.multiply(rate));
        return response;
    }

    private ClientResponse getClientResponse(CurrencyConversionRequest request) {
        Collections.shuffle(clients);
        ClientResponse response = null;
        for (ConversionClient client : clients) {
            try {
                response = client.getRates(request);
            } catch (Exception e) {
                log.warn("Failed to get rates from provider " + client.getProviderName(), e);
            }
        }
        if (response == null) {
            // todo add proper exceptions and exception handling
            throw new RuntimeException("There are no available providers for this operation");
        }
        return response;
    }

}
