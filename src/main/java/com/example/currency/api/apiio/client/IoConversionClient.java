package com.example.currency.api.apiio.client;

import com.example.currency.client.ConversionClient;
import com.example.currency.model.CurrencyConversionRequest;
import org.springframework.web.client.RestTemplate;

// todo add tests
public class IoConversionClient extends ConversionClient {

    private final String url;
    private final String name;

    public IoConversionClient(RestTemplate restTemplate, String url, String name) {
        super(restTemplate);
        this.url = url;
        this.name = name;
    }

    @Override
    public String getExchangeUrl(CurrencyConversionRequest request) {
        return url + request.getFrom();
    }

    @Override
    public String getProviderName() {
        return name;
    }
}
