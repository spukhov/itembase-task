package com.example.currency.api.apiio.config;

import com.example.currency.api.apiio.client.IoConversionClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Configuration
public class IoClientConfig {

    @Value("${api.io-client.url}")
    private String ioClientUrl;
    @Value("${api.io-client.name}")
    private String name;

    @Resource
    private RestTemplate restTemplate;

    @Bean
    public IoConversionClient ioConversionClient() {
        return new IoConversionClient(restTemplate, ioClientUrl, name);
    }
}
