package com.example.currency.api.apicom.config;

import com.example.currency.api.apicom.client.ComConversionClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Configuration
public class ComClientConfig {

    @Value("${api.com-client.url}")
    private String comClientUrl;
    @Value("${api.com-client.name}")
    private String comClientName;

    @Resource
    private RestTemplate restTemplate;

    @Bean
    public ComConversionClient comConversionClient() {
        return new ComConversionClient(restTemplate, comClientUrl, comClientName);
    }
}
