package com.example.currency.api.apicom.client;

import com.example.currency.client.ConversionClient;
import com.example.currency.model.CurrencyConversionRequest;
import org.springframework.web.client.RestTemplate;

// todo add tests
public class ComConversionClient extends ConversionClient {

    private final String url;
    private final String name;

    public ComConversionClient(RestTemplate restTemplate, String url, String name) {
        super(restTemplate);
        this.url = url;
        this.name = name;
    }

    @Override
    public String getExchangeUrl(CurrencyConversionRequest request) {
        return url + request.getFrom();
    }

    @Override
    public String getProviderName() {
        return name;
    }
}
